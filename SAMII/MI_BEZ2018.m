function information = ...
    MI_BEZ2018(transmitted_path, perceived_path, audiogram)
%

%% Check GPU Availability
useGPU = false;
try  
    gpuDevice;
    hBEZ2018 = str2func("BEZ2018_GPU");
    useGPU = true;
catch
    hBEZ2018 = str2func("BEZ2018");
end

%% BEZ2018 Parameters

ag_fs = [125 250 500 1e3 2e3 4e3 8e3];
ag_dbloss = [0 0 0 0 0 0 0]; % No hearing loss

numcfs = 25; % number of critical bands
CFs = logspace(log10(125),log10(8e3),numcfs); % Speech frecuency range;
if useGPU  
    numsponts_healthy = [16 23 61]; % Number of low-spont, medium-spont, 
                                    % and high-spont fibers at each CF in
                                    % a healthy ANF (100 ANFs)
                                    % (Well known ratio in literature,
                                    % Moore (2003))
else
    numsponts_healthy = [1 1 3]; % Original BEZ2018 ratio.
end

nFibers = sum(numsponts_healthy, 'all');

%% Get spike trains
% read audio
[trans_audio, fs_t] = audioread(transmitted_path);
[per_audio, fs_p] = audioread(perceived_path);

% Audio signals should be both binaural or both monoaural 
assert(size(trans_audio,2) == size(per_audio,2));
assert(size(trans_audio,2) <= 2);

% is it binaural?
binaural = false;
if size(trans_audio,2) == 2
    binaural = true;
end
    
% Obtain spike trains of target stimulus
spt_trans = struct();
spt_per = struct();

if binaural
    disp('Computing spike trains for S')
    [spt_trans.left,spt_trans.t] = hBEZ2018(trans_audio(:,1), fs_t, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    
    [spt_trans.right,~] = hBEZ2018(trans_audio(:,2), fs_t, ag_fs, ...
        ag_dbloss, CFs, numsponts_healthy);
    
    % Obtain spike trains of perceived stimulus with hearing loss
    disp('Computing spike trains for R')
    [spt_per.left,spt_per.t] = hBEZ2018(per_audio(:,1), fs_p, ...
        audiogram.cfs, audiogram.left, CFs, numsponts_healthy);
    
    [spt_per.right,~] = hBEZ2018(per_audio(:,2), fs_p, ...
        audiogram.cfs, audiogram.right, CFs, numsponts_healthy);
    
    disp('Computing mutual information and entropies')
    information = libSAMII.binauralMI(spt_trans, spt_per, numcfs, nFibers, CFs);

else
    disp('Computing spike trains for S')
    [spt_trans.mono,spt_trans.t] = hBEZ2018(trans_audio, fs_t, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    
    % Obtain spike trains of perceived stimulus with hearing loss
    disp('Computing spike trains for R')
    [spt_per.mono,spt_per.t] = hBEZ2018(per_audio, fs_p, ...
        audiogram.cfs, audiogram.left, CFs, numsponts_healthy);
    
    disp('Computing mutual information and entropies')
    information = libSAMII.monauralMI(spt_trans, spt_per, numcfs, nFibers, CFs);
end


