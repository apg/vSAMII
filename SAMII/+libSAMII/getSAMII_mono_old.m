function samii = getSAMII_mono_old(mi_data, pre_silence)
% CFs = mi_data.CFs;
dt = mi_data.timeStamps;
pre_frames = dt < pre_silence;

Si = mi_data.mono.Si;
%Ri = mi_data.mono.Ri;
mi = mi_data.mono.mi;
%exp_mi = min(Si, Ri);

% Frame selection
Z_thr = mean(Si(:,pre_frames)') + 3*std(Si(:,pre_frames)');
Zi_thr = mean(mi(:,pre_frames)') + 3*std(mi(:,pre_frames)');
%exp_Zi_thr = mean(exp_mi(:,pre_frames)') + 3*std(exp_mi(:,pre_frames)');
Z_fr = Si > Z_thr';
Z_fr(:,pre_frames) = false;
mi_fr = mi > Zi_thr';
%exp_mi_fr = exp_mi > exp_Zi_thr';
Zi_fr = logical(mi_fr .* Z_fr);
%exp_Zi_fr = logical(exp_mi_fr .* Z_fr);

total_samples = sum(sum(Z_fr));
%max_index = sum(Ri(Z_fr));%/total_samples;
samii = sum(mi(Zi_fr))/total_samples;
end

