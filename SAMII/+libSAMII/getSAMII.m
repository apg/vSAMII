function samii = getSAMII(mi_data, pre_silence)

if isfield(mi_data, 'mono')
    samii = libSAMII.getSAMII_mono(mi_data, pre_silence);
else
    samii_bin = libSAMII.getSAMII_binaural();
    samii = samii_bin.best;
end

