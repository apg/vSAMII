function information = monauralMI(spt_S, spt_R, numcfs, nFibers, CFs)

% Solve any mismatch due to different audio sample frequencies
size_S = length(spt_S.t);
size_R = length(spt_R.t);
min_size = min(size_R, size_S);

awinL = 20e-3; % Analysis window [s]
trefmin = 200e-6;
% trefmin = 1/1500; % Phase locking limit
% trefmin = 208.5e-6; % Minimun refractory period possible in BEZ2018 [s].

% Analysis window and integration window sizes in frames
samplesT = spt_S.t(2); % Periodicity of the spike train samples
iSamples = floor(trefmin/samplesT); % Samples in an integration frame 
                                    % (2 spikes cannot belong to the same
                                    % AN fiber within these samples)
awSamples = iSamples*floor(awinL/(iSamples*samplesT)); % Total samples in 
                                                       % an analysis window
awHop = round(awSamples/2); % Hop size
awTotal = ceil(min_size/awHop); % Total of analysis windows

% Output structure
information = struct();
information.CFs = CFs;
information.timeStamps = (0:awTotal-1)*awHop*samplesT;
information.mono = struct();
information.mono.mi = zeros(numcfs, awTotal);
information.mono.Si = zeros(numcfs, awTotal);
information.mono.Ri = zeros(numcfs, awTotal);

for n=1:awTotal
    % Get frames
    init_frame = 1+(n-1)*awHop;
    end_frame = min(init_frame+awSamples-1, min_size);

    for cf=1:numcfs
        
        % Time integration
        S_m = reduce(spt_S.mono(cf,init_frame:end_frame), iSamples);
        R_m = reduce(spt_R.mono(cf,init_frame:end_frame), iSamples);
        
        if CFs(cf) < 1/trefmin
            time_shift = 1/CFs(cf);
            samp_shift = ceil(time_shift/trefmin);
            best_mi = 0;
            for shift_R = 0:samp_shift-1
                mi_tmp = my_mutualinfo(S_m, circshift(R_m,-shift_R), nFibers);
                if mi_tmp > best_mi
                    best_mi = mi_tmp;
                end
            end
            information.mono.mi(cf,n) = best_mi;
        else
            % Mutual information for every cf (left, rigth and binaural)
            information.mono.mi(cf,n) = my_mutualinfo(S_m, R_m, nFibers);
        end
        % Transmited information of the anechoic signal
        information.mono.Si(cf,n) = my_entropy(S_m, nFibers);

        % Perceived information of the ha signa
        information.mono.Ri(cf,n) = my_entropy(R_m, nFibers);
    end

end

end

function ent = my_entropy(a,nF)
    rho_s = sum(a,'all')/(numel(a)*nF);
    if rho_s == 0 || rho_s == 1
        ent = 0;
    else
        ent = - (rho_s*log2(rho_s) + (1-rho_s)*log2(1-rho_s));
    end
end

function mi = my_mutualinfo(x, y, nF)
    assert(length(x)==length(y))
    N = length(x);
    jpdf = zeros(2,2);
    for n=1:N
        o11 = min(x(n), y(n)); % Ocurrences of [1 1]
        o01 = max(0,y(n)-x(n)); % Ocurrences of [0 1]
        o10 = max(0,x(n)-y(n)); % Ocurrences of [1 0]
        o00 = nF - o11 - o01 - o10; % Ocurrences of [0 0]
        jpdf = jpdf + [o00, o01 ; ...
                       o10, o11];
    end
    jpdf = jpdf/(N*nF);
    ent_x = my_entropy(x, nF);
    ent_y = my_entropy(y, nF);
    jent_xy = - [jpdf(1,1)*log2(jpdf(1,1)), ...
                 jpdf(1,2)*log2(jpdf(1,2)), ...
                 jpdf(2,1)*log2(jpdf(2,1)), ...
                 jpdf(2,2)*log2(jpdf(2,2))];
    jent_xy(isnan(jent_xy)) = 0;
    mi = ent_x + ent_y - sum(jent_xy, 'all');
end

function reduced_spikes = reduce(spikes, frames)
    N = ceil(length(spikes)/frames);
    reduced_spikes = zeros(1, N);
    for n=0:N-1
        initf = n*frames + 1;
        endf = min(initf+frames-1, length(spikes));
        reduced_spikes(n+1) = sum(spikes(initf:endf));
    end
end