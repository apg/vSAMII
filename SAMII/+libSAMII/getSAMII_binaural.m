function samii = getSAMII_binaural(mi_data, pre_silence)
% CFs = mi_data.CFs;
dt = mi_data.timeStamps;
pre_frames = dt < pre_silence;
tags = ["left" "right" "binatural"];

for tag = tags
    Si = mi_data.(tag).Si;
    Ri = mi_data.(tag).Ri;
    mi = mi_data.(tag).mi;
    %expected_mi = min(Si, Ri);

    % Frame selection
    Z_thr = mean(Si(:,pre_frames)') + 3*std(Si(:,pre_frames)');
    Zi_thr = mean(mi(:,pre_frames)') + 3*std(mi(:,pre_frames)');
    Z_fr = Si > Z_thr';
    mi_fr = mi > Zi_thr';
    Zi_fr = logical(mi_fr .* Z_fr);

    total_samples = sum(sum(Z_fr));
    max_index = sum(Ri(Zi_fr))/total_samples;
    mi_index = sum(mi(Zi_fr))/total_samples;

    samii.(tag) = mi_index / max_index;
end

samii.best = max([samii.left samii.right samii.binaural]);
end
