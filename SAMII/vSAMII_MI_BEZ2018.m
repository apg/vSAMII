function information = ...
    vSAMII_MI_BEZ2018(clean_input, noisy_input, TasNet_input, DeepACE_input, fs, pre_silence)
%

%% Check GPU Availability
useGPU = true;
try
    gpuDevice;
catch
    useGPU = false;
end

%% BEZ2018 Parameters

ag_fs = [125 250 500 1e3 2e3 4e3 8e3];
ag_dbloss = [0 0 0 0 0 0 0]; % No hearing loss

numcfs = 25; % number of critical bands
CFs = logspace(log10(125),log10(8e3),numcfs); % Speech frecuency range;
if useGPU  
    numsponts_healthy = [16 23 61]; % Number of low-spont, medium-spont, 
                                    % and high-spont fibers at each CF in
                                    % a healthy ANF (100 ANFs)
                                    % (Well known ratio in literature,
                                    % Moore (2003))
else
    numsponts_healthy = [1 1 3]; % Original BEZ2018 ratio.
end

nFibers = sum(numsponts_healthy, 'all');

%% Get spike trains

% Obtain spike trains of target stimulus
spt_clean = struct();
spt_noisy = struct();
spt_TasNet = struct();
spt_DeepACE = struct();

%Adjust vocoder output signal to Pa scale assuming the clean signal at 
%65 dBSPL
stimdB = 65;
pre_samples = fs*pre_silence;
conv2Pa = 20e-6*10^(stimdB/20)/rms(clean_input(pre_samples+1:end));

clean_input = clean_input*conv2Pa;
noisy_input = noisy_input*conv2Pa;
TasNet_input = TasNet_input*conv2Pa;
DeepACE_input = DeepACE_input*conv2Pa;

disp('Computing spike trains for: ')

if useGPU
    disp(' - Clean')
    [spt_clean.mono,spt_clean.t] = BEZ2018_GPU(clean_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - Noisy')
    [spt_noisy.mono,spt_noisy.t] = BEZ2018_GPU(noisy_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - TasNet')
    [spt_TasNet.mono,spt_TasNet.t] = BEZ2018_GPU(TasNet_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - DeepACE')
    [spt_DeepACE.mono,spt_DeepACE.t] = BEZ2018_GPU(DeepACE_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);

else
    disp(' - Clean')
    [spt_clean.mono,spt_clean.t] = BEZ2018(clean_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - Noisy')
    [spt_noisy.mono,spt_noisy.t] = BEZ2018(noisy_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - TasNet')
    [spt_TasNet.mono,spt_TasNet.t] = BEZ2018(TasNet_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
    disp(' - DeepACE')
    [spt_DeepACE.mono,spt_DeepACE.t] = BEZ2018(DeepACE_input, fs, ...
        ag_fs, ag_dbloss, CFs, numsponts_healthy);
end

% plot_neurogram(spt_clean, CFs, 'Reference Neurogram')
% plot_neurogram(spt_DeepACE, CFs, 'Degraded Neurogram')

%% Get information from spike trains
disp('Computing mutual information and entropies for with NEW implementation')
information = struct();
disp(' - Noisy')
information.noisy = libSAMII.monauralMI(spt_clean, spt_noisy, numcfs, nFibers, CFs);
disp(' - TasNet')
information.TasNet = libSAMII.monauralMI(spt_clean, spt_TasNet, numcfs, nFibers, CFs);
disp(' - DeepACE')
information.DeepACE = libSAMII.monauralMI(spt_clean, spt_DeepACE, numcfs, nFibers, CFs);

disp('Computing mutual information and entropies for with OLD implementation')
disp(' - Noisy')
information.noisy_old = libSAMII.monauralMI_old(spt_clean, spt_noisy, numcfs, nFibers, CFs);
disp(' - DeepACE')
information.DeepACE_old = libSAMII.monauralMI_old(spt_clean, spt_DeepACE, numcfs, nFibers, CFs);
end

function plot_neurogram(spt, CFs, plt_title)
    fig = figure();
    fig.Position = [0 0 350 250];
    dt = spt.t(2);
    resolution = 0.01;
    reduce_factor = round(resolution/dt);
    sp = reduce(spt.mono, reduce_factor);
    t = 0:resolution:(length(sp)-1)*resolution;
    h = pcolor(t,CFs,sp);
    set(h,'LineStyle','none')
    title(plt_title, 'fontsize',12)
    xlabel('Time [s]', 'fontsize',10)
    ylabel('Characteristic Frequency [Hz]', 'fontsize',10)
    cb = colorbar('eastoutside');
    cb.Label.String = 'Spike Count';
    cb.Label.FontSize = 10;
end

function outsignal = reduce(insignal, frames)
    M = size(insignal, 1);
    L = size(insignal, 2);
    N = ceil(L/frames);
    outsignal = zeros(M, N);
    for m=1:M
        for n=0:N-1
            initf = n*frames + 1;
            endf = min(initf+frames-1, L);
            outsignal(m, n+1) = sum(insignal(m, initf:endf));
        end
    end
end
