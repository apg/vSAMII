folder = pwd;
addpath([folder filesep 'SAMII'])
addpath([folder filesep 'BEZ2018_CUDA'])
addpath(genpath([folder filesep 'NMT']))

outfolder = [folder filesep 'mutualinfo'];
if not(isfolder(outfolder))
    mkdir(outfolder)
end

data_folder = [folder filesep 'data'];

noise_types = ["CCITT" "ICRA7"];
SNRs = [0 5];
pre_silence = 2.0;

nHSMlists = 30; % max 30;
n_sentencesxlist = 20; % max 20;

p = ACE_map;
v = RFVocoder(p);
for noise = noise_types
    noise_folder = [data_folder filesep convertStringsToChars(noise)];
    noise_outfolder = [outfolder filesep convertStringsToChars(noise)];
    if not(isfolder(noise_outfolder))
        mkdir(noise_outfolder)
    end
    for l = 1:nHSMlists
        for s = 1:n_sentencesxlist
            name_clean = convertStringsToChars(sprintf('HSMm%02d%02d_SNR=15_CCITT_clean_LGF.mat',l,s));
            path_clean = [noise_folder filesep 'Unprocessed' filesep name_clean];
            load(path_clean);
            clean = Process_from_lgf(p, lgf);
            clean_vocoded = Process(v, clean);
            for SNR = SNRs
                fname = convertStringsToChars(sprintf('HSMm%02d%02d_SNR=%d_%s_mutualinfo.json',l,s,SNR,noise));
                disp(['****************** Computing: ' fname ' ******************'])
                fout = [noise_outfolder filesep fname];
                if isfile(fout)
                    % If the scene has been already computed, skip it.
                    continue
                end
                % Unprocessed
                name_noisy = convertStringsToChars(sprintf('HSMm%02d%02d_SNR=%d_%s_mixed_LGF.mat',l,s,SNR,noise));
                path_noisy = [noise_folder filesep 'Unprocessed' filesep name_noisy];
                load(path_noisy);
                noisy = Process_from_lgf(p, lgf);
                noisy_vocoded = Process(v, noisy);
                
                % TasNet
                name_TasNet = convertStringsToChars(sprintf('HSMm%02d%02d_SNR=%d_%s_mixed_denoised.mat',l,s,SNR,noise));
                path_TasNet = [noise_folder filesep 'TasNet' filesep name_TasNet];
                load(path_TasNet);
                TasNet = Process_from_lgf(p, lgf);
                TasNet_vocoded = Process(v, TasNet);

                % DeepACE
                name_DeepACE = convertStringsToChars(sprintf('HSMm%02d%02d_SNR=%d_%s_mixed_denoised.mat',l,s,SNR,noise));
                path_DeepACE = [noise_folder filesep 'DeepACE' filesep name_DeepACE];
                load(path_DeepACE);
                DeepACE = Process_from_lgf(p, lgf);
                DeepACE_vocoded = Process(v, DeepACE);
                
                % Mutual information  
                output = vSAMII_MI_BEZ2018(clean_vocoded, noisy_vocoded, TasNet_vocoded, DeepACE_vocoded, p.audio_sample_rate, pre_silence);

                % Create .json file
                txt = jsonencode(output);
                fid = fopen(fout, 'wt');
                fprintf(fid,"%s",txt);
                fclose(fid);
            end
        end
    end
end


