folder = pwd;
addpath([folder filesep 'SAMII'])

mi_path = [folder filesep 'mutualinfo'];

noise_types = ["CCITT" "ICRA7"];
SNRs = [0 5];
pre_silence = 2.0;

Results = struct();
Results_old = struct();
Results.header = ["Unprocessed" "TasNet" "DeepACE"];
Results_old.header = ["Unprocessed" "DeepACE"];

for noise = noise_types
    Results.(noise) = struct();
    Results_old.(noise) = struct();
    for snr = SNRs
        Results.(noise).([ 'dBsnr' num2str(snr)]) = [];
        Results_old.(noise).([ 'dBsnr' num2str(snr)]) = [];
    end
    noise_folder = [mi_path filesep convertStringsToChars(noise)];
    json_list = dir(noise_folder);
    n_files = length(json_list);
    for jidx = 1:n_files
        json_obj = json_list(jidx);
        filename = [noise_folder filesep json_obj.name];
        [fPath, fName, fExt] = fileparts(filename);
        if ~strcmp(fExt,".json")
            continue
        end
        disp(fName)
        mi_data = readJSON(filename);
        expression = 'SNR=([0-9]+)_';
        tokens = regexp(fName,expression,'tokens');
        SNR = tokens{1}{1};
        samii_noisy = libSAMII.getSAMII_mono(mi_data.noisy, pre_silence);
        samii_TasNet = libSAMII.getSAMII_mono(mi_data.TasNet, pre_silence);
        samii_DeepACE = libSAMII.getSAMII_mono(mi_data.DeepACE, pre_silence);
        samii_noisy_old = libSAMII.getSAMII_mono_old(mi_data.noisy_old, pre_silence);
        samii_DeepACE_old = libSAMII.getSAMII_mono_old(mi_data.DeepACE_old, pre_silence);
        Results.(noise).([ 'dBsnr' SNR]) = [Results.(noise).([ 'dBsnr' SNR]); samii_noisy samii_TasNet samii_DeepACE];
        Results_old.(noise).([ 'dBsnr' SNR]) = [Results_old.(noise).([ 'dBsnr' SNR]); samii_noisy_old samii_DeepACE_old];       
    end
    
end

outfolder = [folder filesep 'Results'];
if not(isfolder(outfolder))
    mkdir(outfolder)
end

fout = [outfolder filesep 'results.json'];
fout_old = [outfolder filesep 'results_old.json'];

txt = jsonencode(Results);
fid = fopen(fout, 'wt');
fprintf(fid,"%s",txt);
fclose(fid);

txt = jsonencode(Results_old);
fid = fopen(fout_old, 'wt');
fprintf(fid,"%s",txt);
fclose(fid);

function data = readJSON(fname)
    fid = fopen(fname); 
    raw = fread(fid,inf); 
    str = char(raw'); 
    fclose(fid); 
    data = jsondecode(str);
end