%% Initialization
folder = pwd;
addpath([folder filesep 'SAMII'])
addpath([folder filesep 'BEZ2018_CUDA'])

data_folder = [folder filesep 'test_data'];

%% File selection
clean_speech_path = [data_folder filesep 'vocoded_clean.wav'];
degraded_speech_path = [data_folder filesep 'vocoded_noisy.wav'];
denoised_speech_path = [data_folder filesep 'vocoded_denoised.wav'];

%% Listener audiogram (Simulation of hearing loss)
audiogram.cfs = [125 250 500 1e3 2e3 4e3 8e3];
audiogram.left = [0 0 0 0 0 0 0]; % No hearing loss
audiogram.right = [0 0 0 0 0 0 0]; % No hearing loss
audiogram.mono = [0 0 0 0 0 0 0]; % No hearing loss

%% Call SAMII+BEZ2018 handler
disp('--- Noisy Signal ---')
output_noisy = MI_BEZ2018(clean_speech_path, degraded_speech_path, audiogram);
disp('--- Denoised Signal ---')
output_denoised = MI_BEZ2018(clean_speech_path, denoised_speech_path, audiogram);

%% Calculate SAMII
disp('--- Computing SAMII ---')
pre_silence = 2.0; % test_data have a 2 second presilence
samii_noisy = libSAMII.getSAMII(output_noisy, pre_silence);
samii_denoised = libSAMII.getSAMII(output_denoised, pre_silence);
disp(['SAMII noisy: ' num2str(samii_noisy) ' - SAMII denoised: ' num2str(samii_denoised)])

%% Generate some images
if isfield(output_noisy, 'mono')
    ch = "mono";
else
    ch = "binaural";
end

figure();
subplot(3,1,1)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_noisy.(ch).Si);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Information in Clean Speech', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;

subplot(3,1,2)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_noisy.(ch).Ri);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Information in Noisy Speech', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;

subplot(3,1,3)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_noisy.(ch).mi);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Mutual Information between Noisy and Clean', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;

figure();
subplot(3,1,1)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_denoised.(ch).Si);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Information in Clean Speech', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;

subplot(3,1,2)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_denoised.(ch).Ri);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Information in Denoised Speech', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;

subplot(3,1,3)
h = pcolor(output_noisy.timeStamps, output_noisy.CFs, output_denoised.(ch).mi);
set(h, 'EdgeColor', 'none');
set(gca, 'YScale', 'log')
c = colorbar;
title('Mutual Information between Denoised and Clean', 'fontsize',24)
xlabel('Time [s]', 'fontsize',16)
ylabel('Frequency [Hz]', 'fontsize',16)
c.Label.String = 'Entropy [bits]';
c.Label.FontSize = 16;