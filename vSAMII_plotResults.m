clear
clc
folder = pwd;

results_path = [folder filesep 'Results'];

sdatafile = [folder filesep 'data' filesep 'subject_data.mat'];
resultsfile = [results_path filesep 'results.json'];

Results = readJSON(resultsfile);
load(sdatafile)
%SNRs = [0 5 10 15];
SNRs = [0 5];

nPerf = size(Performances,1);
fit_data = [];
data_by_PandSCS = struct();
%p_idx = 0;
for p = 1:nPerf
    performance = Performances(p, :);
    nlist = performance(1);
    dBsnr = performance(2);
    scs_key = performance(3);
    noise_key = performance(4);
    patient_key = performance(5);
    score = performance(6);
    
    nScore = score / CleanPerformances(patient_key);
    nScore = min(1.0, nScore);
    noise = noise_types(noise_key);
    scs = sc_strats(scs_key);
%     if strcmp(scs, "Wiener")
%         continue
%     end
    for scs_idx = 1:4
        if strcmp(scs, Results.header(scs_idx))
            break
        end
    end
    l_init = (nlist-1)*20 + 1;
    try
        samii = mean(Results.(noise).([ 'dBsnr' num2str(dBsnr)])(l_init:l_init+19,scs_idx) );
        %p_idx = p_idx + 1;
        fit_data(:,end+1) = [nScore samii];
        if ~isfield(data_by_PandSCS, ['p' num2str(patient_key)])
            data_by_PandSCS.(['p' num2str(patient_key)]) = struct();
        end
        if ~isfield(data_by_PandSCS.(['p' num2str(patient_key)]), scs)
            data_by_PandSCS.(['p' num2str(patient_key)]).(scs) = [];
        end
        data_by_PandSCS.(['p' num2str(patient_key)]).(scs)(:,end+1) = [nScore samii];
    catch
        
    end
end

y = fit_data(1,:);
x = fit_data(2,:);

% Fitting a line
lm = fitlm(x, y);
xLin = linspace(0, 1, 1000);
yLin = predict(lm , xLin');

% Fitting a sigmoid
xSig = linspace(0, 1, 1000);
modelfun = @(b, x) 1./(1+exp(-b(1)*(x-b(2))));
start = [1000 0.5];
nlm = fitnlm(x, y, modelfun, start);
b1 = nlm.Coefficients.Estimate(1);
b2 = nlm.Coefficients.Estimate(2);
ySig = predict(nlm, xSig');

fig = figure();
fig.Position = [0 0 850 450];
plot(xSig, 100*ySig, 'g-', 'LineWidth', 2); % Plot fitted sigmoid.
hold on
%plot(xLin, 100*yLin, 'r-', 'LineWidth', 2); % Plot fitted line.
plot(x, 100*y, 'b.', 'MarkerSize', 15); % Plot training data.
xlim([0.3 0.8])
ylim([0 100])
filename = [results_path filesep 'fitting.png'];
title('Linear and sigmoid regression from patient data', 'fontsize',24)
subtitle('Performance is normalized to subject performances @ inf SNR using ACE', 'fontsize',12)
xlabel('SAMII', 'fontsize',16)
ylabel('Subject performance [% words correct]', 'fontsize',14)
%legend({['Sigmoid fit (R^2 = ' sprintf('%0.2f', nlm.Rsquared.adjusted) ')'], ['Linear fit (R^2 = ' sprintf('%0.2f', lm.Rsquared.adjusted) ')'], 'CI users data' }, 'Location','best','fontsize',12);
legend({['Sigmoid fit (R^2 = ' sprintf('%0.2f', nlm.Rsquared.adjusted) ')'], 'CI users data' }, 'Location','best','fontsize',12);
grid on;
exportgraphics(fig,filename,'Resolution',600)
close(fig)

pFields = fields(data_by_PandSCS);
npat = length(pFields);
scs_markers = ['o'; '+'; '^'; 'x'];
for pidx = 1:npat
    fig = figure();
    fig.Position = [0 0 850 450];
    patient = pFields{pidx};
    scsFields = fields(data_by_PandSCS.(patient));
    nscs = length(scsFields);
    x_fit = [];
    y_fit = [];
    for scsi = 1:nscs
        scs = scsFields{scsi};
        x_pat = data_by_PandSCS.(patient).(scs)(2,:);
        y_pat = data_by_PandSCS.(patient).(scs)(1,:);
        style = ['b' scs_markers(scsi)];
        plot(x_pat, 100*y_pat, style, 'MarkerSize', 8); % Plot training data.
        hold on
        x_fit = [x_fit x_pat];
        y_fit = [y_fit y_pat];
    end
    
    % Fitting a line
    lm = fitlm(x_fit, y_fit);
    xLin = linspace(0, 1, 1000);
    yLin = predict(lm , xLin');

    % Fitting a sigmoid
    xSig = linspace(0, 1, 1000);
    modelfun = @(b, x) 1./(1+exp(-b(1)*(x-b(2))));
    start = [1000 0.5];
    nlm = fitnlm(x_fit, y_fit, modelfun, start);
    b1 = nlm.Coefficients.Estimate(1);
    b2 = nlm.Coefficients.Estimate(2);
    ySig = predict(nlm, xSig');
    
    plot(xLin, 100*yLin, 'r-', 'LineWidth', 2); % Plot fitted line.
    plot(xSig, 100*ySig, 'g-', 'LineWidth', 2); % Plot fitted sigmoid.
    xlim([0.3 0.8])
    ylim([0 100])
    filename = [results_path filesep 'fitting' patient '.png'];
    title(['Linear and sigmoid regression for patient ' num2str(pidx)], 'fontsize',24)
    subtitle('Performance is normalized to subject performances @ inf SNR using ACE', 'fontsize',12)
    xlabel('SAMII', 'fontsize',16)
    ylabel('Subject performance [% words correct]', 'fontsize',14)
    %legend({scsFields{1}, scsFields{2}, scsFields{3}, ['Linear fit (R^2 = ' sprintf('%0.2f', lm.Rsquared.adjusted) ')'], ['Sigmoid fit (R^2 = ' sprintf('%0.2f', nlm.Rsquared.adjusted) ')'] }, 'Location','best','fontsize',12);
    legend({scsFields{1}, scsFields{2}, scsFields{3}, scsFields{4}, ['Linear fit (R^2 = ' sprintf('%0.2f', lm.Rsquared.adjusted) ')'], ['Sigmoid fit (R^2 = ' sprintf('%0.2f', nlm.Rsquared.adjusted) ')'] }, 'Location','best','fontsize',12);
    grid on;
    exportgraphics(fig,filename,'Resolution',600)
    close(fig)
end

for noise = noise_types
    for snr = SNRs
        fig = figure();
        fig.Position = [0 0 850 450];
        boxplot(Results.(noise).([ 'dBsnr' num2str(snr)]))
        filename = [results_path filesep 'boxplot' convertStringsToChars(noise) 'dBsnr' num2str(snr) '.png'];
        title('Performance of denoising algorithms', 'fontsize',24)
        subtitle(['Noise: ' convertStringsToChars(noise) '@ SNR = ' num2str(snr) ' dB'], 'fontsize',20)
        xlabel('Sound coding strategy', 'fontsize',16)
        %xticklabels({'ACE', 'TasNet+ACE', 'DeepACE'})
        xticklabels({'ACE', 'TasNet+ACE', 'Wiener+ACE', 'Deep ACE'})
        ylabel('SAMII', 'fontsize',16)
        exportgraphics(fig,filename,'Resolution',600)
        close(fig)
    end
end

function data = readJSON(fname)
    fid = fopen(fname); 
    raw = fread(fid,inf); 
    str = char(raw'); 
    fclose(fid); 
    data = jsondecode(str);
end