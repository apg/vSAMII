# vSAMII

This repository contains the newest version of the Spike Activity Mutual Information Index (SAMII). vSAMII is the implementation of SAMII to predict the performance of different denoising sound coding strategies ([Deep ACE](10.1109/TBME.2023.3262677), [Wiener+ACE](10.3109/14992027.2016.1172267) and [TasNet+ACE](10.1109/TASLP.2019.2915167)) using a vocoder to simulate the signal that reaches the human auditory system.

## Block Diagram
![SAMII](./SAMII/SAMII_v2.png)

## Getting started

In this repository can be found the original code used for the experiments described in the poster presented at the [CIAP conference 2023](http://ciaphome.org/). They can be identifed by the prefix "vSAMII" in their names. However, the data, the vocoder implementation and the sound coding strategies are not here.

In order to execute and test SAMII, we offer an [example](https://gitlab.gwdg.de/apg/computational-models/vSAMII/-/blob/main/example.m). Before executing this code please follow the [compilation steps](https://gitlab.gwdg.de/apg/computational-models/vSAMII/-/blob/main/BEZ2018_CUDA/readme.txt) for the peripheral auditory model.

## Support

Please contact Franklin Y. Alvarez
alvarez.franklin@mh-hannover.de
